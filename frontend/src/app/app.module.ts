import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgxsModule } from '@ngxs/store';
import { NgxsStoragePluginModule } from '@ngxs/storage-plugin';
import { NgxsReduxDevtoolsPluginModule } from '@ngxs/devtools-plugin';
import { environment } from 'src/environments/environment';
import { SessionState } from './shared/services/storage/session/session.state';
import { ErrorComponent } from './views/pages/error/error.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { Interceptor } from './shared/services/interceptors/error.interceptor';
import { ApiService } from './shared/services/api/api.service';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MenuButtonComponent } from './views/pages/super/children/school-list/components/menu-button/menu-button.component';
import { MatMenuModule } from '@angular/material/menu';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { ArrivedDialogComponent } from './shared/components/arrived-dialog/arrived-dialog.component';
import { MatDialogModule } from '@angular/material/dialog';
import { NgxMaskIonicModule } from 'ngx-mask-ionic';

@NgModule({
  declarations: [AppComponent, ErrorComponent, ArrivedDialogComponent],
  entryComponents: [],
  imports: [
    NgxMaskIonicModule.forRoot(),
    HttpClientModule,
    MatDialogModule,
    MatMenuModule,
    MatIconModule,
    MatButtonModule,
    BrowserModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    BrowserAnimationsModule,
    NgxsModule.forRoot([SessionState], {
      developmentMode: !environment.production,
    }),
    NgxsStoragePluginModule.forRoot({
      key: ['session'],
    }),
    NgxsReduxDevtoolsPluginModule.forRoot(),
    MatSnackBarModule,
  ],
  providers: [
    ApiService,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: Interceptor,
      multi: true,
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
