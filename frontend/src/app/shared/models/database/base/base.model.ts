import { Id } from "./id.model";

export abstract class BaseModel {
  
  id?: Id = null
  createdAt?: Date = null
  updatedAt?: Date = null
}