import { BaseModel } from './base/base.model';
import { School } from './school.model';

export class User extends BaseModel {
  public accessToken: string = null;
  public name?: string = null;
  public cpf?: string = null;
  public relatedSchoolCNPJ?: string = null;
  public relatedParentCPF?: string = null;
  public classroom?: { name: string; period: string; description: string } =
    null;
  public roles?: string[] = null;
  public school?: School = null;
}
