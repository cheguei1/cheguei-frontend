import { User } from 'src/app/shared/models/database/user.model';

export class SessionStateModel {
  user?: User = null;
}
