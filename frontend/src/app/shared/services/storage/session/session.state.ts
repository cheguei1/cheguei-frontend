import { Injectable } from "@angular/core";
import { Action, State, StateContext } from "@ngxs/store";
import { LogOut, SetUser } from "./session.actions";
import { SessionStateModel } from "./session.model";

const initialState: SessionStateModel = {
    user: null
}

@State<SessionStateModel>({
    name: 'session',
    defaults: initialState
})
@Injectable()
export class SessionState {

    @Action(SetUser)
    setUser({ patchState }: StateContext<SessionStateModel>, { payload }: SetUser) {
        patchState({ user: payload })
    }

    @Action(LogOut)
    logOut({ patchState }: StateContext<SessionStateModel>) {
        patchState(initialState)
    }
}