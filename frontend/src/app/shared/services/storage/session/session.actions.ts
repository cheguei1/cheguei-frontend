import { User } from "src/app/shared/models/database/user.model";

export class SetUser {

    static readonly type = '[User] Set'

    constructor(public payload: User){}
}

export class LogOut {

    static readonly type = '[User] LogOut'

    constructor(){}
}