import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngxs/store';
import { User } from '../../models/database/user.model';
import { LogOut, SetUser } from './session/session.actions';

@Injectable({
  providedIn: 'root'
})
export class StorageService {

  constructor(
    private store: Store,
    private router: Router
  ) { }

  logout(): void {
    this.store.dispatch(new LogOut())
    this.router.navigate(['/auth', 'login'])
  }

  get user(): User {
    return this.store.snapshot().session.user
  }

  set user(value: User) {
    this.logout()
    this.store.dispatch(new SetUser(value))
  }

  get accessToken(): string {
    return this.store.snapshot().session.accessToken
  }
}
