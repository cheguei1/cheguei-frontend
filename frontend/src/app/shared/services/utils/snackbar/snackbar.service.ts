import { Injectable } from '@angular/core';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material/snack-bar';

@Injectable({
  providedIn: 'root'
})
export class SnackbarService {

  private matSnackbarConfig: MatSnackBarConfig<any> = {
    duration: 6000,
    horizontalPosition: 'center',
    verticalPosition: 'bottom'
  }

  constructor(private matSnackbar: MatSnackBar) { }

  show(message: string) {
    this.matSnackbar.open(message, 'OK', this.matSnackbarConfig)
  }

  showError(message: string) {
    this.matSnackbar.open(message, 'OK', { ...this.matSnackbarConfig, panelClass: 'error-snackbar' })
  }
}
