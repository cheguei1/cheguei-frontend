import { Injectable } from '@angular/core';
import { RouterClass } from './paths.routes';

@Injectable({
  providedIn: 'root'
})
export class RouterService {

  constructor() { }

  get routers() {
    return RouterClass.routers
  }
}
