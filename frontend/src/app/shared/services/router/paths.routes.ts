export class RouterClass {

    static readonly paths = {
        AUTH: {
            value: 'auth',
            root: '/auth',
            children: {
                LOGIN: 'login',
                REGISTER: 'register',
                FORGET_PASSWORD: 'forget-password'
            }
        },
        SUPER: {
            value: 'super',
            root: '/super',
            children: {
                SCHOOL_LIST: 'school-list',
                ADMIN_LIST: 'admin-list'
            }
        }
    }

    static readonly routers = {
        AUTH: {
            LOGIN: [
                RouterClass.paths.AUTH.root,
                RouterClass.paths.AUTH.children.LOGIN
            ],
            REGISTER: [
                RouterClass.paths.AUTH.root,
                RouterClass.paths.AUTH.children.REGISTER
            ],
            FORGET_PASSWORD: [
                RouterClass.paths.AUTH.root,
                RouterClass.paths.AUTH.children.FORGET_PASSWORD
            ]
        },
        SUPER: {
            SCHOOL_LIST: [
                RouterClass.paths.SUPER.root,
                RouterClass.paths.SUPER.children.SCHOOL_LIST
            ],
            ADMIN_LIST: [
                RouterClass.paths.SUPER.root,
                RouterClass.paths.SUPER.children.ADMIN_LIST
            ]
        }
    }
}