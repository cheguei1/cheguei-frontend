import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpErrorResponse,
  HttpHeaders,
} from '@angular/common/http';
import { empty, Observable } from 'rxjs';
import { Router } from '@angular/router';
import { catchError } from 'rxjs/operators';
import { StorageService } from '../storage/storage.service';

@Injectable()
export class Interceptor implements HttpInterceptor {
  constructor(private router: Router, private storageService: StorageService) {}

  intercept(
    request: HttpRequest<unknown>,
    next: HttpHandler
  ): Observable<HttpEvent<unknown>> {
    if (request.url.includes('user/login')) return next.handle(request);

    const authReq = request.clone({
      headers: new HttpHeaders(
        `Authorization: Bearer ${this.storageService.user.accessToken}`
      ),
    });

    return next.handle(authReq).pipe(
      catchError((error: HttpErrorResponse) => {
        this.router.navigate(['error', 500]);
        return empty();
      })
    );
  }
}
