import { AbstractControl, ValidationErrors } from "@angular/forms"

export class CustomValidators {
    static complexPassword(formControl: AbstractControl): ValidationErrors | null {
        const condition = /^(?:(?=.*[a-z])(?:(?=.*[A-Z])(?=.*[\d\W])|(?=.*\W)(?=.*\d))|(?=.*\W)(?=.*[A-Z])(?=.*\d)).{8,}$/
        return formControl.value && condition.test(formControl.value) ? null : { complexpassword: true }
    }

    static confirmPassword(formGroup: AbstractControl): { [key: string]: any } | null {
        const password: boolean = formGroup.get('password')!.value;
        const confirmPassword: boolean = formGroup.get('confirmPassword')!.value; 
        return password === confirmPassword ? null : { different: true }
      }
}