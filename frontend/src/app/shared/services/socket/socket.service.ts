import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { BehaviorSubject } from 'rxjs';
import { io } from 'socket.io-client';
import { ArrivedDialogComponent } from '../../components/arrived-dialog/arrived-dialog.component';
import { StorageService } from '../storage/storage.service';

@Injectable({
  providedIn: 'root',
})
export class SocketService {
  baseUrl = 'https://cheguei-api.azurewebsites.net/';
  socket = null;

  requests$ = new BehaviorSubject([]);

  constructor(
    private storeService: StorageService,
    private dialog: MatDialog
  ) {}

  socketConnect() {
    this.socket = io(this.baseUrl, {
      reconnection: true,
    });

    console.log(this.storeService.user.cpf);
    console.log(this.storeService.user.school.cnpj);
    this.socket.emit('login', {
      cpf: this.storeService.user.cpf,
      schoolRelatedCNPJ: this.storeService.user.school.cnpj,
    });
    this.socket.on('arrived', (data) => {
      console.log(data);
      if (
        !this.requests$.value.find(
          (value) => value.parent.cpf === data.parent.cpf
        )
      ) {
        this.requests$.next([...this.requests$.value, data]);
        this.dialog.open(ArrivedDialogComponent, {
          width: '250px',
          data: { ...data },
        });
      }
    });
  }

  finalizeRequest(cpf) {
    const index = this.requests$.value.findIndex(
      (value) => value.parent.cpf === cpf
    );
    console.log(index);
    const value = [...this.requests$.value];
    const newArray = value.splice(index, 1);
    console.log(newArray);
    this.requests$.next(value);
    console.log(this.requests$.value);
    this.socket.emit('delivered', {
      relatedParentCPF: cpf,
      schoolRelatedCNPJ: this.storeService.user.school.cnpj,
    });
  }
}
