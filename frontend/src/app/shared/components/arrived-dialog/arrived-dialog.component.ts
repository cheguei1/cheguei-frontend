import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { School } from 'src/app/shared/models/database/school.model';

@Component({
  selector: 'app-arrived-dialog',
  templateUrl: './arrived-dialog.component.html',
  styleUrls: ['./arrived-dialog.component.scss'],
})
export class ArrivedDialogComponent {
  constructor(
    public dialogRef: MatDialogRef<ArrivedDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  formatChildrenNames() {
    return this.data.children.reduce((prevValue, nextValue) => {
      return (prevValue = `${nextValue.name} ,  ${prevValue}`);
    }, '');
  }
}
