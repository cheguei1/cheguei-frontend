import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CustomInputComponent } from './custom-input/custom-input.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { ConfirmationDialogComponent } from './confirmation-dialog/confirmation-dialog.component';
import { MatDialogModule } from '@angular/material/dialog';
import { MatButtonModule } from '@angular/material/button';
import { LogoutButtonComponent } from './logout-button/logout-button.component';
import { MatIconModule } from '@angular/material/icon';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MenuButtonComponent } from 'src/app/views/pages/super/children/school-list/components/menu-button/menu-button.component';
import { MatMenuModule } from '@angular/material/menu';
import { NgxMaskIonicModule } from 'ngx-mask-ionic';

@NgModule({
  declarations: [
    CustomInputComponent,
    ConfirmationDialogComponent,
    LogoutButtonComponent,
    MenuButtonComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    MatDialogModule,
    MatButtonModule,
    MatIconModule,
    MatTooltipModule,
    NgxMaskIonicModule,
    MatMenuModule,
  ],
  exports: [CustomInputComponent, LogoutButtonComponent, MenuButtonComponent],
})
export class ComponentsModule {}
