import { ChangeDetectorRef, Component } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { filter } from 'rxjs/operators';
import { StorageService } from './shared/services/storage/storage.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  isAdmin = false;
  isSuper = false;
  other;
  constructor(
    private router: Router,
    public storageService: StorageService,
    private cdr: ChangeDetectorRef
  ) {}

  ngOnInit() {
    this.router.events
      .pipe(filter((e) => e instanceof NavigationEnd))
      .subscribe((e: any) => {
        if (e.url.includes('super')) {
          this.isAdmin = false;
          this.isSuper = true;
          this.other = false;
          this.cdr.markForCheck();
          return;
        }
        if (e.url.includes('admin')) {
          this.isAdmin = true;
          this.isSuper = false;
          this.other = false;
          this.cdr.markForCheck();
          return;
        }

        this.isAdmin = false;
        this.isSuper = false;
        this.other = true;
        this.cdr.markForCheck();
      });
  }

  navigate(route: string[]) {
    this.router.navigate(route);
  }

  logout() {
    this.storageService.logout();
  }
}
