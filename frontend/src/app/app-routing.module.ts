import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './shared/services/guards/auth/auth.guard';
import { ErrorComponent } from './views/pages/error/error.component';
import { HomeComponent } from './views/pages/home/home.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'auth',
    pathMatch: 'full',
  },
  {
    path: 'auth',
    loadChildren: () =>
      import('./views/pages/auth/auth.module').then((m) => m.AuthModule),
  },
  {
    path: 'super',
    loadChildren: () =>
      import('./views/pages/super/super.module').then((m) => m.SuperModule),
  },
  {
    path: 'admin',
    loadChildren: () =>
      import('./views/pages/admin/admin.module').then((m) => m.AdminModule),
  },
  {
    path: 'responsible',
    loadChildren: () =>
      import('./views/pages/responsible/responsible.module').then(
        (m) => m.ResponsibleModule
      ),
  },
  {
    path: 'monitor',
    loadChildren: () =>
      import('./views/pages/monitor/monitor.module').then(
        (m) => m.MonitorModule
      ),
  },
  {
    path: 'home',
    component: HomeComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'error/:type',
    component: ErrorComponent,
  },
  {
    path: '**',
    redirectTo: 'error',
    pathMatch: 'full',
  },
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      preloadingStrategy: PreloadAllModules,
      onSameUrlNavigation: 'reload',
    }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
