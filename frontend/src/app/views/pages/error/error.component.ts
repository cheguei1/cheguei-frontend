import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { StorageService } from 'src/app/shared/services/storage/storage.service';

@Component({
  selector: 'app-error',
  templateUrl: './error.component.html',
  styleUrls: ['./error.component.scss'],
})
export class ErrorComponent implements OnInit {

  type: number
  title: string
  desc: string

  constructor(
    private route: ActivatedRoute,
    public storage: StorageService
  ) { }

  ngOnInit() {
    this.type = Number(this.route.snapshot.paramMap.get('type'));
    switch (this.type) {
      case 404:
        this.title = 'Página não encontrada.'
        this.desc = 'Parece que essa página não existe...'
        break;

      case 500:
        this.title = 'Erro desconhecido'
        this.desc = 'Parece que ocorreu um erro, por favor tente novamente mais tarde.'
        break;

      default:
        this.type = 418
        this.title = 'Erro desconhecido'
        this.desc = 'Parece que ocorreu um erro, por favor tente novamente mais tarde.'
        break;
    }
  }
}