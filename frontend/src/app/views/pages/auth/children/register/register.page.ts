import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { CustomValidators } from 'src/app/shared/services/form/custom-validators.class';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {

  registerFormGroup: FormGroup = null

  constructor(
    private formBuilder: FormBuilder
  ) { }

  ngOnInit() {
    this.setForm()
  }

  registerEmployee(): void {
    console.log(this.registerFormGroup.value)
  }

  getFormControl(formControl: string) : FormControl {
    if(!formControl) {
      throw new Error('Erro: nome do formControl não passado para a função.')
    }
    return this.registerFormGroup.get(formControl) as FormControl
  }

  private setForm(): void {
    this.registerFormGroup = this.formBuilder.group({
      name: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, CustomValidators.complexPassword]],
      confirmPassword: ['', [Validators.required, CustomValidators.complexPassword]],
    }, {
      validators: [CustomValidators.confirmPassword]
    })
  }
}
