import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import jwtDecode from 'jwt-decode';
import { take } from 'rxjs/operators';
import { ApiService } from 'src/app/shared/services/api/api.service';
import { RouterService } from 'src/app/shared/services/router/router.service';
import { StorageService } from 'src/app/shared/services/storage/storage.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  constructor(
    private apiService: ApiService,
    private storageService: StorageService,
    private router: Router,
    private routerService: RouterService
  ) {}

  loginFormGroup = new FormGroup(
    {
      username: new FormControl('', [Validators.required]),
      password: new FormControl('', [Validators.required]),
    },
    {}
  );

  ngOnInit() {}

  getFormControl(formControl: string): FormControl {
    if (!formControl) {
      throw new Error('Erro: nome do formControl não passado para a função.');
    }
    return this.loginFormGroup.get(formControl) as FormControl;
  }

  login() {
    this.apiService
      .post('user/login', this.loginFormGroup.getRawValue())
      .pipe(take(1))
      .subscribe((data) => {
        const accessToken = data.accessToken;
        this.storageService.user = {
          ...jwtDecode(accessToken),
          accessToken: data.accessToken,
        };
        console.log(this.storageService.user);
        console.log(this.storageService.user.roles[0]);

        if (this.storageService.user.roles[0] === 'super') {
          this.router.navigate(['super']);
        } else if (this.storageService.user.roles[0] === 'admin') {
          this.router.navigate(['admin']);
        } else if (this.storageService.user.roles[0] === 'parent') {
          this.router.navigate(['responsible']);
        } else if (this.storageService.user.roles[0] === 'monitor') {
          this.router.navigate(['monitor']);
        }
      });
  }
}
