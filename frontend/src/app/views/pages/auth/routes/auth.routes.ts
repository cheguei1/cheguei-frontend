import { Routes } from '@angular/router';
import { LoginPage } from '../children/login/login.page';
import { RegisterPage } from '../children/register/register.page';
import { authPaths } from './auth-routes.paths';

export const authRoutes: Routes = [
    {
      path: '',
      redirectTo: authPaths.LOGIN,
      pathMatch: 'full'
    },
    {
        path: authPaths.LOGIN,
        component: LoginPage
    },
    {
        path: authPaths.REGISTER,
        component: RegisterPage
    }
  ];