import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { authRoutes } from './routes/auth.routes';
import { RegisterPage } from './children/register/register.page';
import { LoginPage } from './children/login/login.page';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { ComponentsModule } from 'src/app/shared/components/components.module';
import { ApiService } from 'src/app/shared/services/api/api.service';

@NgModule({
  declarations: [RegisterPage, LoginPage],
  imports: [
    CommonModule,
    RouterModule.forChild(authRoutes),
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    ComponentsModule,
  ],
})
export class AuthModule {}
