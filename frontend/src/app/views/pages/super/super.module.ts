import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { SchoolRegisterPage } from './children/school-register/school-register.page';
import { SchoolListPage } from './children/school-list/school-list.page';
import { MenuButtonComponent } from './children/school-list/components/menu-button/menu-button.component';
import { SchoolProfileModalComponent } from './children/school-list/components/menu-button/components/school-profile-modal/school-profile-modal.component';
import { AdminModalComponent } from './children/admin-list/admin-profile-modal/admin-profile-modal.component';
import { SchoolEditPage } from './children/school-edit/school-edit.page';
import { AdminListComponent } from './children/admin-list/admin-list.page';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { ComponentsModule } from 'src/app/shared/components/components.module';
import { schoolRoutes } from './routes/school.routes';
import { MatTableModule } from '@angular/material/table';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { MatDialogModule } from '@angular/material/dialog';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { AdminRegisterComponent } from './children/admin-register/admin-register.component';
import { AdminEditComponent } from './children/admin-edit/admin-edit.component';

@NgModule({
  declarations: [
    SchoolRegisterPage,
    SchoolListPage,
    SchoolEditPage,

    SchoolProfileModalComponent,
    AdminModalComponent,
    AdminListComponent,
    AdminRegisterComponent,
    AdminEditComponent,
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(schoolRoutes),
    MatMenuModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    ComponentsModule,
    MatTableModule,
    MatButtonModule,
    MatIconModule,

    MatDialogModule,
    MatProgressSpinnerModule,
  ],
})
export class SuperModule {}
