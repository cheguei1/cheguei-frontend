export const schoolPaths = {
  REGISTER: 'school-register',
  LIST: 'school-list',
  EDIT: 'school-edit',
  ADMIN_LIST: 'admin-list',
  ADMIN_REGISTER: 'admin-register',
  ADMIN_EDIT: 'admin-edit',
};
