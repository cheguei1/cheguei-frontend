import { Routes } from '@angular/router';
import { AdminEditComponent } from '../children/admin-edit/admin-edit.component';
import { AdminListComponent } from '../children/admin-list/admin-list.page';
import { AdminRegisterComponent } from '../children/admin-register/admin-register.component';
import { SchoolEditPage } from '../children/school-edit/school-edit.page';
import { SchoolListPage } from '../children/school-list/school-list.page';
import { SchoolRegisterPage } from '../children/school-register/school-register.page';

import { schoolPaths } from './school-routes.paths';

export const schoolRoutes: Routes = [
  {
    path: '',
    redirectTo: schoolPaths.LIST,
    pathMatch: 'full',
  },
  {
    path: schoolPaths.REGISTER,
    component: SchoolRegisterPage,
  },
  {
    path: schoolPaths.LIST,
    component: SchoolListPage,
  },
  {
    path: `${schoolPaths.EDIT}/:schoolCNPJ`,
    component: SchoolEditPage,
  },
  {
    path: `${schoolPaths.ADMIN_LIST}/:cnpj`,
    component: AdminListComponent,
  },
  {
    path: `${schoolPaths.ADMIN_REGISTER}/:cnpj`,
    component: AdminRegisterComponent,
  },
  {
    path: `${schoolPaths.ADMIN_EDIT}/:cpf`,
    component: AdminEditComponent,
  },
];
