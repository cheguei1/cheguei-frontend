import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { Router } from '@angular/router';
import { debounceTime, take } from 'rxjs/operators';
import { ApiService } from 'src/app/shared/services/api/api.service';
import { SnackbarService } from 'src/app/shared/services/utils/snackbar/snackbar.service';

@Component({
  selector: 'school-register',
  templateUrl: './school-register.page.html',
  styleUrls: ['./school-register.page.scss'],
})
export class SchoolRegisterPage implements OnInit {
  schoolRegisterFormGroup = new FormGroup(
    {
      name: new FormControl('', [Validators.required]),
      cnpj: new FormControl('', [Validators.required]),
      address: new FormControl('', [Validators.required]),
    },
    {}
  );

  constructor(
    private router: Router,
    private apiService: ApiService,
    private snackbarService: SnackbarService
  ) {}

  ngOnInit() {}

  registerSchool(): void {
    this.apiService
      .post('school', this.schoolRegisterFormGroup.getRawValue())
      .pipe(take(1), debounceTime(500))
      .subscribe(() => {
        this.snackbarService.show('Escola cadastrada com sucesso');
        this.router.navigate(['super', 'school-list']);
      });
  }

  getFormControl(formControl: string): FormControl {
    if (!formControl) {
      throw new Error('Erro: nome do formControl não passado para a função.');
    }
    return this.schoolRegisterFormGroup.get(formControl) as FormControl;
  }

  return() {
    this.router.navigate(['super', 'school-list']);
  }
}
