import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { switchMap, take, tap } from 'rxjs/operators';
import { ConfirmationDialogComponent } from 'src/app/shared/components/confirmation-dialog/confirmation-dialog.component';
import { ApiService } from 'src/app/shared/services/api/api.service';
import { SnackbarService } from 'src/app/shared/services/utils/snackbar/snackbar.service';
import { SchoolProfileModalComponent } from './components/menu-button/components/school-profile-modal/school-profile-modal.component';

@Component({
  selector: 'school-list',
  templateUrl: './school-list.page.html',
  styleUrls: ['./school-list.page.scss'],
})
export class SchoolListPage implements OnInit {
  displayedColumns: string[] = ['name', 'options'];
  dataSource = [];

  loadItems() {
    return this.apiService.get('school').pipe(
      take(1),
      tap((schools) => (this.dataSource = schools))
    );
  }

  menuButtons: {
    buttonName: string;
    buttonAction: (element?: any) => any;
  }[] = [
    {
      buttonName: 'Informações',
      buttonAction: (element) => {
        this.dialog.open(SchoolProfileModalComponent, {
          width: '250px',
          data: { ...element },
        });
      },
    },
    {
      buttonName: 'Administradores',
      buttonAction: (element) => {
        this.router.navigate([
          'super',
          'admin-list',
          element.cnpj.replace(/\D/g, ''),
        ]);
      },
    },
    {
      buttonName: 'Editar',
      buttonAction: (element) => {
        this.router.navigate([
          'super',
          'school-edit',
          element.cnpj.replace(/\D/g, ''),
        ]);
      },
    },
    {
      buttonName: 'Excluir',
      buttonAction: (element) => {
        const dialog = this.dialog.open(ConfirmationDialogComponent, {
          width: '250px',
        });
        dialog.afterClosed().subscribe((result) => {
          if (result) {
            this.apiService
              .delete(`school?cnpj=${element.cnpj}`)
              .pipe(
                switchMap(() => this.loadItems()),
                take(1)
              )
              .subscribe(() => {
                this.snackbarService.show('Escola excluida com sucesso');
              });
          }
        });
      },
    },
  ];

  constructor(
    private apiService: ApiService,
    private router: Router,
    private dialog: MatDialog,
    private snackbarService: SnackbarService,
    private cdr: ChangeDetectorRef
  ) {}

  ngOnInit() {
    this.loadItems().subscribe(() => {
      this.cdr.markForCheck();
    });
  }
  ionViewWillEnter() {
    this.loadItems().subscribe(() => {
      this.cdr.markForCheck();
    });
  }

  newSchool() {
    this.router.navigate(['super', 'school-register']);
  }
}
