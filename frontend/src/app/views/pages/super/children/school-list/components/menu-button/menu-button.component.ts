import { Component, Input, OnInit } from '@angular/core';

import { RouterService } from 'src/app/shared/services/router/router.service';

@Component({
  selector: 'app-menu-button',
  templateUrl: './menu-button.component.html',
  styleUrls: ['./menu-button.component.scss'],
})
export class MenuButtonComponent implements OnInit {
  @Input('element') element: any;
  @Input('buttons') buttons: {
    buttonName: string;
    buttonAction: (element?: any) => any;
  }[] = [];

  constructor(public routerService: RouterService) {}

  ngOnInit() {
    if (!this.element) {
      throw new Error('Item não repassado para o componente de menu.');
    }
  }
}
