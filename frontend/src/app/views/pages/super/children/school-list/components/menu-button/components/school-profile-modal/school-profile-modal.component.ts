import { Component, Inject } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { School } from "src/app/shared/models/database/school.model";

@Component({
    selector: 'app-school-profile-modal',
    templateUrl: './school-profile-modal.component.html',
    styleUrls: ['./school-profile-modal.component.scss']
})
export class SchoolProfileModalComponent {

    constructor(
        public dialogRef: MatDialogRef<SchoolProfileModalComponent>,
        @Inject(MAT_DIALOG_DATA) public data: School,
    ) { }
}