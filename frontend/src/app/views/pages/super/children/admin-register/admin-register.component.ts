import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { take } from 'rxjs/operators';
import { ApiService } from 'src/app/shared/services/api/api.service';
import { SnackbarService } from 'src/app/shared/services/utils/snackbar/snackbar.service';

@Component({
  templateUrl: './admin-register.component.html',
  styleUrls: ['./admin-register.page.scss'],
})
export class AdminRegisterComponent {
  adminRegisterFormGroup = new FormGroup(
    {
      name: new FormControl('', [Validators.required]),
      cpf: new FormControl('', [Validators.required]),
    },
    {}
  );

  constructor(
    private router: Router,
    private apiService: ApiService,
    private route: ActivatedRoute,
    private snackbarService: SnackbarService
  ) {}

  ngOnInit() {}

  registerSchool(): void {
    this.apiService
      .post('user', {
        ...this.adminRegisterFormGroup.getRawValue(),
        role: 'admin',
        password: '123456',
        relatedSchoolCNPJ: this.route.snapshot.paramMap
          .get('cnpj')
          .replace(/(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})/, '$1.$2.$3/$4-$5'),
      })
      .pipe(take(1))
      .subscribe(() => {
        this.snackbarService.show('Usuário cadastrado com sucesso');
        this.router.navigate([
          'super',
          'admin-list',
          this.route.snapshot.paramMap.get('cnpj'),
        ]);
      });
  }

  getFormControl(formControl: string): FormControl {
    if (!formControl) {
      throw new Error('Erro: nome do formControl não passado para a função.');
    }
    return this.adminRegisterFormGroup.get(formControl) as FormControl;
  }

  return() {
    this.router.navigate([
      'super',
      'admin-list',
      this.route.snapshot.paramMap.get('cnpj'),
    ]);
  }
}
