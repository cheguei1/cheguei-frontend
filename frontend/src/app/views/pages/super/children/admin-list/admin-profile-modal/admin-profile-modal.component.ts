import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { School } from 'src/app/shared/models/database/school.model';
import { User } from 'src/app/shared/models/database/user.model';

@Component({
  selector: 'app-admin-profile-modal',
  templateUrl: './admin-profile-modal.component.html',
  styleUrls: ['./admin-profile-modal.component.scss'],
})
export class AdminModalComponent {
  constructor(
    public dialogRef: MatDialogRef<AdminModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: User
  ) {}
}
