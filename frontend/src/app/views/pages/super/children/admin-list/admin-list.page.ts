import { ChangeDetectorRef, Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { filter, map, switchMap, take, tap } from 'rxjs/operators';
import { ConfirmationDialogComponent } from 'src/app/shared/components/confirmation-dialog/confirmation-dialog.component';
import { ApiService } from 'src/app/shared/services/api/api.service';
import { SnackbarService } from 'src/app/shared/services/utils/snackbar/snackbar.service';
import { AdminModalComponent } from './admin-profile-modal/admin-profile-modal.component';

@Component({
  templateUrl: './admin-list.page.html',
  styleUrls: ['./admin-list.page.scss'],
})
export class AdminListComponent {
  displayedColumns: string[] = ['name', 'options'];
  dataSource = [];

  loadItems() {
    return this.apiService
      .get(
        `user?cnpj=${decodeURIComponent(
          this.route.snapshot.paramMap
            .get('cnpj')
            .replace(/(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})/, '$1.$2.$3/$4-$5')
        )}`
      )
      .pipe(
        map(
          (users) => users.filter((user) => user.roles?.includes('admin')) ?? []
        ),
        tap((users) => (this.dataSource = users)),
        take(1)
      );
  }

  menuButtons: {
    buttonName: string;
    buttonAction: (element?: any) => any;
  }[] = [
    {
      buttonName: 'Informações',
      buttonAction: (element) => {
        this.dialog.open(AdminModalComponent, {
          width: '250px',
          data: { ...element },
        });
      },
    },
    {
      buttonName: 'Editar',
      buttonAction: (element) => {
        this.router.navigate([
          'super',
          'admin-edit',
          element.cpf.replace(/\D/g, ''),
        ]);
      },
    },
    {
      buttonName: 'Excluir',
      buttonAction: (element) => {
        const dialog = this.dialog.open(ConfirmationDialogComponent, {
          width: '250px',
        });
        dialog.afterClosed().subscribe((result) => {
          if (result) {
            this.apiService
              .delete(`user/${element.cpf}`)
              .pipe(
                switchMap(() => this.loadItems()),
                take(1)
              )
              .subscribe(() =>
                this.snackbarService.show('Usuário excluido com sucesso')
              );
          }
        });
      },
    },
  ];

  constructor(
    private dialog: MatDialog,
    private apiService: ApiService,
    private router: Router,
    private route: ActivatedRoute,
    private cdr: ChangeDetectorRef,
    private snackbarService: SnackbarService
  ) {}

  ngOnInit() {
    this.loadItems().subscribe(() => {
      this.cdr.markForCheck();
    });
  }
  ionViewWillEnter() {
    this.loadItems().subscribe(() => {
      this.cdr.markForCheck();
    });
  }

  newAdmin() {
    this.router.navigate([
      'super',
      'admin-register',
      this.route.snapshot.paramMap.get('cnpj'),
    ]);
  }
}
