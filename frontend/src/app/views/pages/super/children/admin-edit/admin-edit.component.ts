import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { take } from 'rxjs/operators';
import { ApiService } from 'src/app/shared/services/api/api.service';
import { SnackbarService } from 'src/app/shared/services/utils/snackbar/snackbar.service';

@Component({
  templateUrl: './admin-edit.component.html',
  styleUrls: ['./admin-edit.page.scss'],
})
export class AdminEditComponent {
  cnpj;
  adminEditFormGroup = new FormGroup(
    {
      name: new FormControl('', [Validators.required]),
      cpf: new FormControl('', [Validators.required]),
    },
    {}
  );

  constructor(
    private router: Router,
    private apiService: ApiService,
    private route: ActivatedRoute,
    private snackbarService: SnackbarService
  ) {}

  ngOnInit() {
    this.apiService
      .get(
        `user/${this.route.snapshot.paramMap
          .get('cpf')
          .replace(/(\d{3})(\d{3})(\d{3})(\d{2})/, '$1.$2.$3-$4')}`
      )
      .pipe(take(1))
      .subscribe((user) => {
        this.adminEditFormGroup.patchValue(user);
        console.log(user);
        this.cnpj = user.school.cnpj;
      });
  }

  editAdmin(): void {
    this.apiService
      .put(
        `user/${this.route.snapshot.paramMap
          .get('cpf')
          .replace(/(\d{3})(\d{3})(\d{3})(\d{2})/, '$1.$2.$3-$4')}`,
        {
          ...this.adminEditFormGroup.getRawValue(),
        }
      )
      .pipe(take(1))
      .subscribe(() => {
        this.snackbarService.show('Usuário editado com sucesso');
        this.router.navigate([
          'super',
          'admin-list',
          this.cnpj.replace(/\D/g, ''),
        ]);
      });
  }

  getFormControl(formControl: string): FormControl {
    if (!formControl) {
      throw new Error('Erro: nome do formControl não passado para a função.');
    }
    return this.adminEditFormGroup.get(formControl) as FormControl;
  }

  return() {
    this.router.navigate([
      'super',
      'admin-list',
      this.adminEditFormGroup
        .getRawValue()
        .relatedSchoolCNPJ.replace(/\D/g, ''),
    ]);
  }
}
