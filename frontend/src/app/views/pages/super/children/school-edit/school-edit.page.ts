import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { take } from 'rxjs/operators';
import { ApiService } from 'src/app/shared/services/api/api.service';
import { SnackbarService } from 'src/app/shared/services/utils/snackbar/snackbar.service';

@Component({
  selector: 'school-edit',
  templateUrl: './school-edit.page.html',
  styleUrls: ['./school-edit.page.scss'],
})
export class SchoolEditPage implements OnInit {
  schoolEditFormGroup = new FormGroup(
    {
      name: new FormControl('', [Validators.required]),
      cnpj: new FormControl('', [Validators.required]),
      address: new FormControl('', [Validators.required]),
    },
    {}
  );

  constructor(
    private apiService: ApiService,
    private router: Router,
    private route: ActivatedRoute,
    private snackbarService: SnackbarService
  ) {}

  ngOnInit() {
    this.apiService
      .get(
        `school?cnpj=${this.route.snapshot.paramMap
          .get('schoolCNPJ')
          .replace(/(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})/, '$1.$2.$3/$4-$5')}`
      )
      .pipe(take(1))
      .subscribe((school) => {
        this.schoolEditFormGroup.patchValue(school);
      });
  }

  editSchool(): void {
    this.apiService
      .put(
        `school?cnpj=${this.route.snapshot.paramMap
          .get('schoolCNPJ')
          .replace(/(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})/, '$1.$2.$3/$4-$5')}`,
        this.schoolEditFormGroup.getRawValue()
      )
      .pipe(take(1))
      .subscribe(() => {
        this.snackbarService.show('Escola editada com sucesso');
        this.router.navigate(['super', 'school-list']);
      });
  }

  getFormControl(formControl: string): FormControl {
    if (!formControl) {
      throw new Error('Erro: nome do formControl não passado para a função.');
    }
    return this.schoolEditFormGroup.get(formControl) as FormControl;
  }
  return() {
    this.router.navigate(['super', 'school-list']);
  }
}
