import { Routes } from "@angular/router";
import { ResponsibleComponent } from "./responsible.component";

export const responsibleRoutes: Routes = [
    {
        path: '',
        component: ResponsibleComponent
    }
]