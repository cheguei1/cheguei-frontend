import { Component, OnInit } from '@angular/core';
import { take } from 'rxjs/operators';
import { ApiService } from 'src/app/shared/services/api/api.service';
import { StorageService } from 'src/app/shared/services/storage/storage.service';
import { SnackbarService } from 'src/app/shared/services/utils/snackbar/snackbar.service';

@Component({
  selector: 'app-responsible',
  templateUrl: './responsible.component.html',
  styleUrls: ['./responsible.component.scss'],
})
export class ResponsibleComponent implements OnInit {
  constructor(
    private apiService: ApiService,
    private snackbarService: SnackbarService,
    public storageService: StorageService
  ) {}

  ngOnInit() {
    console.log(this.storageService.user);
    console.log(this.storageService.user.cpf);
    console.log(this.storageService.user.school.cnpj);
  }

  arrived(): void {
    this.apiService
      .post('notification', {})
      .pipe(take(1))
      .subscribe(() => {
        this.snackbarService.show('Notificação enviada para a escola');
      });
  }
}
