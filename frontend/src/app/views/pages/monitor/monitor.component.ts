import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { take } from 'rxjs/operators';
import { ArrivedDialogComponent } from 'src/app/shared/components/arrived-dialog/arrived-dialog.component';
import { ApiService } from 'src/app/shared/services/api/api.service';
import { SocketService } from 'src/app/shared/services/socket/socket.service';
import { StorageService } from 'src/app/shared/services/storage/storage.service';
import { SnackbarService } from 'src/app/shared/services/utils/snackbar/snackbar.service';

@Component({
  selector: 'app-monitor',
  templateUrl: './monitor.component.html',
  styleUrls: ['./monitor.component.scss'],
})
export class MonitorComponent implements OnInit {
  displayedColumns: string[] = ['name', 'options'];
  dataSource$ = this.socketService.requests$;

  menuButtons: {
    buttonName: string;
    buttonAction: (element?: any) => any;
  }[] = [
    {
      buttonName: 'Informações',
      buttonAction: (element) => {
        this.dialog.open(ArrivedDialogComponent, {
          width: '250px',
          data: { ...element },
        });
      },
    },

    {
      buttonName: 'Finalizar',
      buttonAction: (element) => {
        this.socketService.finalizeRequest(element.parent.cpf);
        this.snackbarService.show('Alunos entregues');
      },
    },
  ];

  constructor(
    private socketService: SocketService,
    private dialog: MatDialog,
    private snackbarService: SnackbarService
  ) {}

  ngOnInit() {
    this.socketService.socketConnect();
  }
}
