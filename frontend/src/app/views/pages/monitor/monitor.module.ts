import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { MonitorComponent } from './monitor.component';
import { monitorRoutes } from './monitor.routes';
import { MatTableModule } from '@angular/material/table';
import { ComponentsModule } from 'src/app/shared/components/components.module';

@NgModule({
  declarations: [MonitorComponent],
  imports: [
    CommonModule,
    MatButtonModule,
    MatTableModule,
    ComponentsModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(monitorRoutes),
  ],
})
export class MonitorModule {}
