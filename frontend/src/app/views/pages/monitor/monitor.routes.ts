import { Routes } from '@angular/router';
import { MonitorComponent } from './monitor.component';

export const monitorRoutes: Routes = [
  {
    path: '',
    component: MonitorComponent,
  },
];
