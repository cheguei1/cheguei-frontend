import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { ComponentsModule } from 'src/app/shared/components/components.module';

import { adminPath } from './routes/admin.routes';
import { ClassRoomRegisterPage } from './children/classroom-register/classroom-register.page';
import { MatTableModule } from '@angular/material/table';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { ClassroomListPage } from './children/classroom-list/classroom-list.page';
import { MenuButtonComponent } from '../super/children/school-list/components/menu-button/menu-button.component';
import { MatDialog, MatDialogModule } from '@angular/material/dialog';
import { ClassroomProfileModalComponent } from './children/classroom-list/classroom-profile-modal/classroom-profile-modal.component';
import { ClassRoomEditPage } from './children/classroom-edit/classroom-edit.page';
import { MonitorRegisterComponent } from './children/monitor-register/monitor-register.component';
import { MonitorListComponent } from './children/monitor-list/monitor-list.page';
import { MonitorEditComponent } from './children/monitor-edit/monitor-edit.component';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { ParentListComponent } from './children/parent-list/parent-list.page';
import { ParentRegisterComponent } from './children/parent-register/parent-register.component';

import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { MatOptionModule } from '@angular/material/core';
import { ParentChildrensModalComponent } from './children/parent-list/parent-childrens/parent-childrens-modal.component';

@NgModule({
  declarations: [
    ClassRoomRegisterPage,
    ClassroomProfileModalComponent,
    ClassRoomEditPage,
    ClassroomListPage,

    MonitorRegisterComponent,
    MonitorListComponent,
    MonitorEditComponent,
    ParentListComponent,
    ParentRegisterComponent,
    ParentChildrensModalComponent,
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(adminPath),
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    ComponentsModule,
    MatTableModule,
    MatButtonModule,
    MatIconModule,
    MatDialogModule,
    MatProgressSpinnerModule,
    FormsModule,
    MatFormFieldModule,
    MatSelectModule,
    MatOptionModule,
  ],
})
export class AdminModule {}
