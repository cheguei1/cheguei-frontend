import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { Router } from '@angular/router';
import { take } from 'rxjs/operators';
import { ApiService } from 'src/app/shared/services/api/api.service';
import { StorageService } from 'src/app/shared/services/storage/storage.service';
import { SnackbarService } from 'src/app/shared/services/utils/snackbar/snackbar.service';

@Component({
  selector: 'classroom-register',
  templateUrl: './classroom-register.page.html',
  styleUrls: ['./classroom-register.page.scss'],
})
export class ClassRoomRegisterPage implements OnInit {
  classRoomRegisterFormGroup = new FormGroup(
    {
      name: new FormControl('', [Validators.required]),
      period: new FormControl('', [Validators.required]),
      description: new FormControl('', [Validators.required]),
    },
    {}
  );

  constructor(
    private apiService: ApiService,
    private storageService: StorageService,
    private router: Router,
    private snackbarService: SnackbarService
  ) {}

  ngOnInit() {}

  registerClassroom(): void {
    this.apiService
      .post('school/classroom', {
        ...this.classRoomRegisterFormGroup.getRawValue(),
        relatedSchoolCNPJ: this.storageService.user.school.cnpj,
      })
      .pipe(take(1))
      .subscribe(() => {
        this.snackbarService.show('Classe cadastrada com sucesso');
        this.router.navigate(['admin', 'classroom-list']);
      });
  }

  getFormControl(formControl: string): FormControl {
    if (!formControl) {
      throw new Error('Erro: nome do formControl não passado para a função.');
    }
    return this.classRoomRegisterFormGroup.get(formControl) as FormControl;
  }
}
