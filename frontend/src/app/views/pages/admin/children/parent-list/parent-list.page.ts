import { ChangeDetectorRef, Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { filter, map, switchMap, take, tap } from 'rxjs/operators';
import { ConfirmationDialogComponent } from 'src/app/shared/components/confirmation-dialog/confirmation-dialog.component';
import { ApiService } from 'src/app/shared/services/api/api.service';
import { StorageService } from 'src/app/shared/services/storage/storage.service';
import { SnackbarService } from 'src/app/shared/services/utils/snackbar/snackbar.service';
import { AdminModalComponent } from '../../../super/children/admin-list/admin-profile-modal/admin-profile-modal.component';
import { ParentChildrensModalComponent } from './parent-childrens/parent-childrens-modal.component';

@Component({
  templateUrl: './parent-list.page.html',
  styleUrls: ['./parent-list.page.scss'],
})
export class ParentListComponent {
  displayedColumns: string[] = ['name', 'options'];
  dataSource = [];

  loadItems() {
    return this.apiService
      .get(`user?cnpj=${this.storageService.user.school.cnpj}`)
      .pipe(
        map(
          (users) =>
            users.filter((user) => user.roles?.includes('parent')) ?? []
        ),
        tap((users) => (this.dataSource = users)),
        take(1)
      );
  }

  menuButtons: {
    buttonName: string;
    buttonAction: (element?: any) => any;
  }[] = [
    {
      buttonName: 'Informações',
      buttonAction: (element) => {
        this.dialog.open(ParentChildrensModalComponent, {
          width: '250px',
          data: element.cpf,
        });
      },
    },

    {
      buttonName: 'Excluir',
      buttonAction: (element) => {
        const dialog = this.dialog.open(ConfirmationDialogComponent, {
          width: '250px',
        });
        dialog.afterClosed().subscribe((result) => {
          if (result) {
            this.apiService
              .delete(`user/${element.cpf}`)
              .pipe(
                switchMap(() => this.loadItems()),
                take(1)
              )
              .subscribe(() =>
                this.snackbarService.show('Responsável excluido com sucesso')
              );
          }
        });
      },
    },
  ];

  constructor(
    private dialog: MatDialog,
    private apiService: ApiService,
    private router: Router,
    private storageService: StorageService,
    private snackbarService: SnackbarService,
    private cdr: ChangeDetectorRef
  ) {}

  ngOnInit() {
    this.loadItems().subscribe(() => {
      this.cdr.markForCheck();
    });
  }
  ionViewWillEnter() {
    this.loadItems().subscribe(() => {
      this.cdr.markForCheck();
    });
  }

  newAdmin() {
    this.router.navigate(['admin', 'parent-register']);
  }
}
