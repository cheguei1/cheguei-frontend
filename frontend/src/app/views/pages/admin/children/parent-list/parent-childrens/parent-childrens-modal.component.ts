import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { School } from 'src/app/shared/models/database/school.model';
import { ApiService } from 'src/app/shared/services/api/api.service';

@Component({
  selector: 'app-parent-childrens-modal',
  templateUrl: './parent-childrens-modal.component.html',
  styleUrls: ['./parent-childrens-modal.component.scss'],
})
export class ParentChildrensModalComponent {
  data: any;
  children = [];
  constructor(
    public dialogRef: MatDialogRef<ParentChildrensModalComponent>,
    @Inject(MAT_DIALOG_DATA) public cpf: string,
    private apiService: ApiService
  ) {}

  ngOnInit() {
    this.apiService
      .get(`user/parent/${this.cpf}/children`)
      .subscribe((data) => {
        console.log(data);
        this.data = data;
        this.children = data.children;
      });
  }
}
