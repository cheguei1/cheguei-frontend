import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { take, tap } from 'rxjs/operators';
import { User } from 'src/app/shared/models/database/user.model';
import { ApiService } from 'src/app/shared/services/api/api.service';
import { StorageService } from 'src/app/shared/services/storage/storage.service';
import { SnackbarService } from 'src/app/shared/services/utils/snackbar/snackbar.service';

@Component({
  templateUrl: './parent-register.component.html',
  styleUrls: ['./parent-register.page.scss'],
})
export class ParentRegisterComponent {
  childrens: User[] = [];
  classrooms = [];
  parentRegisterFormGroup = new FormGroup(
    {
      name: new FormControl('', [Validators.required]),
      cpf: new FormControl('', [Validators.required]),
    },
    {}
  );

  constructor(
    private router: Router,
    private apiService: ApiService,
    private storageService: StorageService,
    private snackbarService: SnackbarService
  ) {}

  ngOnInit() {
    this.loadClassrooms().subscribe();
  }

  registerParent(): void {
    this.apiService
      .post('user/bulk', {
        parent: {
          ...this.parentRegisterFormGroup.getRawValue(),
        },
        children: this.childrens.map((children) => {
          return {
            name: children.name,
            cpf: children.cpf,
            classroom: children.classroom,
          };
        }),

        defaultPassword: '123456',
      })
      .pipe(take(1))
      .subscribe(() => {
        this.snackbarService.show(
          'Responsável e filhos cadastrados com sucesso'
        );
        this.router.navigate(['admin', 'parent-list']);
      });
  }

  loadClassrooms() {
    return this.apiService
      .get(`school/classroom?cnpj=${this.storageService.user.school.cnpj}`)
      .pipe(
        take(1),
        tap((data) => (this.classrooms = data)),
        tap(() => console.log(this.classrooms))
      );
  }

  getFormControl(formControl: string): FormControl {
    if (!formControl) {
      throw new Error('Erro: nome do formControl não passado para a função.');
    }
    return this.parentRegisterFormGroup.get(formControl) as FormControl;
  }

  newChild() {
    this.childrens = [...this.childrens, new User()];
    console.log(this.childrens);
  }

  addClassToChild(i, value) {
    this.childrens[i].classroom = value.detail.value;
  }
  setCPF(i, event) {
    this.childrens[i].cpf = event;
  }
  setName(i, event) {
    this.childrens[i].name = event;
  }

  return() {
    this.router.navigate(['admin', 'parent-list']);
  }
}
