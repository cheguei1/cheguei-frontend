import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { take } from 'rxjs/operators';
import { ApiService } from 'src/app/shared/services/api/api.service';
import { StorageService } from 'src/app/shared/services/storage/storage.service';
import { SnackbarService } from 'src/app/shared/services/utils/snackbar/snackbar.service';

@Component({
  templateUrl: './monitor-register.component.html',
  styleUrls: ['./monitor-register.page.scss'],
})
export class MonitorRegisterComponent {
  adminMonitorFormGroup = new FormGroup(
    {
      name: new FormControl('', [Validators.required]),
      cpf: new FormControl('', [Validators.required]),
    },
    {}
  );

  constructor(
    private router: Router,
    private apiService: ApiService,
    private route: ActivatedRoute,
    private storageService: StorageService,
    private snackbarService: SnackbarService
  ) {}

  ngOnInit() {}

  registerMonitor(): void {
    this.apiService
      .post('user', {
        ...this.adminMonitorFormGroup.getRawValue(),
        role: 'monitor',
        password: '123456',
        relatedSchoolCNPJ: this.storageService.user.school.cnpj,
      })
      .pipe(take(1))
      .subscribe(() => {
        this.snackbarService.show('Monitor cadastrado com sucesso');
        this.router.navigate(['admin', 'monitor-list']);
      });
  }

  getFormControl(formControl: string): FormControl {
    if (!formControl) {
      throw new Error('Erro: nome do formControl não passado para a função.');
    }
    return this.adminMonitorFormGroup.get(formControl) as FormControl;
  }

  return() {
    this.router.navigate(['admin', 'monitor-list']);
  }
}
