import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { take } from 'rxjs/operators';
import { ApiService } from 'src/app/shared/services/api/api.service';
import { StorageService } from 'src/app/shared/services/storage/storage.service';
import { SnackbarService } from 'src/app/shared/services/utils/snackbar/snackbar.service';

@Component({
  selector: 'classroom-edit',
  templateUrl: './classroom-edit.page.html',
  styleUrls: ['./classroom-edit.page.scss'],
})
export class ClassRoomEditPage implements OnInit {
  classRoomEditFormGroup = new FormGroup({
    name: new FormControl('', [Validators.required]),
    period: new FormControl('', [Validators.required]),
    description: new FormControl('', [Validators.required]),
  });

  constructor(
    private apiService: ApiService,
    private storageService: StorageService,
    private router: Router,
    private route: ActivatedRoute,
    private snackbarService: SnackbarService
  ) {}

  ngOnInit() {
    this.apiService
      .get(
        `school/classroom/${this.route.snapshot.paramMap.get(
          'name'
        )}/${this.route.snapshot.paramMap.get('period')}?cnpj=${
          this.storageService.user.school.cnpj
        }`
      )
      .pipe(take(1))
      .subscribe((classroom) => {
        this.classRoomEditFormGroup.patchValue(classroom);
      });
  }

  editClassroom(): void {
    this.apiService
      .put(
        `school/classroom/${this.route.snapshot.paramMap.get(
          'name'
        )}/${this.route.snapshot.paramMap.get('period')}?cnpj=${
          this.storageService.user.school.cnpj
        }`,
        {
          ...this.classRoomEditFormGroup.getRawValue(),
        }
      )
      .pipe(take(1))
      .subscribe(() => {
        this.snackbarService.show('Classe editada com sucesso');
        this.router.navigate(['admin', 'classroom-list']);
      });
  }

  getFormControl(formControl: string): FormControl {
    if (!formControl) {
      throw new Error('Erro: nome do formControl não passado para a função.');
    }
    return this.classRoomEditFormGroup.get(formControl) as FormControl;
  }
}
