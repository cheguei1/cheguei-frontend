import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { School } from 'src/app/shared/models/database/school.model';

@Component({
  selector: 'app-classroom-profile-modal',
  templateUrl: './classroom-profile-modal.component.html',
  styleUrls: ['./classroom-profile-modal.component.scss'],
})
export class ClassroomProfileModalComponent {
  constructor(
    public dialogRef: MatDialogRef<ClassroomProfileModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}
}
