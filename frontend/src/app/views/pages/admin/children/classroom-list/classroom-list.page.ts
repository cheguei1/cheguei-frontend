import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { switchMap, take, tap } from 'rxjs/operators';
import { ConfirmationDialogComponent } from 'src/app/shared/components/confirmation-dialog/confirmation-dialog.component';
import { ApiService } from 'src/app/shared/services/api/api.service';
import { StorageService } from 'src/app/shared/services/storage/storage.service';
import { SnackbarService } from 'src/app/shared/services/utils/snackbar/snackbar.service';
import { ClassroomProfileModalComponent } from './classroom-profile-modal/classroom-profile-modal.component';

@Component({
  selector: 'classroom-list',
  templateUrl: './classroom-list.page.html',
  styleUrls: ['./classroom-list.page.scss'],
})
export class ClassroomListPage implements OnInit {
  displayedColumns: string[] = ['name', 'options'];
  dataSource;

  menuButtons: {
    buttonName: string;
    buttonAction: (element?: any) => any;
  }[] = [
    {
      buttonName: 'Informações',
      buttonAction: (element) => {
        this.dialog.open(ClassroomProfileModalComponent, {
          width: '250px',
          data: { ...element },
        });
      },
    },

    {
      buttonName: 'Editar',
      buttonAction: (element) => {
        this.router.navigate([
          'admin',
          'classroom-edit',
          this.storageService.user.school.cnpj,
          element.name,
          element.period,
        ]);
      },
    },
    {
      buttonName: 'Excluir',
      buttonAction: (element) => {
        const dialog = this.dialog.open(ConfirmationDialogComponent, {
          width: '250px',
        });
        dialog.afterClosed().subscribe((result) => {
          if (result) {
            this.apiService
              .delete(
                `school/classroom/${element.name}/${element.period}?cnpj=${this.storageService.user.school.cnpj}`
              )
              .pipe(
                switchMap(() => this.loadItems()),
                take(1)
              )
              .subscribe(() => {
                this.snackbarService.show('Classe excluida com sucesso');
              });
          }
        });
      },
    },
  ];
  constructor(
    private apiService: ApiService,
    private router: Router,
    private storageService: StorageService,
    private dialog: MatDialog,
    private snackbarService: SnackbarService,
    private cdr: ChangeDetectorRef
  ) {}

  loadItems() {
    return this.apiService
      .get(`school/classroom?cnpj=${this.storageService.user.school.cnpj}`)
      .pipe(
        take(1),
        tap((data) => (this.dataSource = data))
      );
  }

  ngOnInit() {
    this.loadItems().subscribe(() => {
      this.cdr.markForCheck();
    });
  }
  ionViewWillEnter() {
    this.loadItems().subscribe(() => {
      this.cdr.markForCheck();
    });
  }

  newClass() {
    this.router.navigate(['admin', 'classroom-register']);
  }
}
