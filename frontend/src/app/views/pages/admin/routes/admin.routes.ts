import { Routes } from '@angular/router';
import { ClassRoomEditPage } from '../children/classroom-edit/classroom-edit.page';
import { ClassroomListPage } from '../children/classroom-list/classroom-list.page';
import { ClassRoomRegisterPage } from '../children/classroom-register/classroom-register.page';
import { MonitorEditComponent } from '../children/monitor-edit/monitor-edit.component';
import { MonitorListComponent } from '../children/monitor-list/monitor-list.page';
import { MonitorRegisterComponent } from '../children/monitor-register/monitor-register.component';
import { ParentListComponent } from '../children/parent-list/parent-list.page';
import { ParentRegisterComponent } from '../children/parent-register/parent-register.component';

import { adminRoutes } from './admin.paths';

export const adminPath: Routes = [
  {
    path: '',
    redirectTo: adminRoutes.LIST,
    pathMatch: 'full',
  },
  {
    path: adminRoutes.REGISTER,
    component: ClassRoomRegisterPage,
  },
  {
    path: adminRoutes.LIST,
    component: ClassroomListPage,
  },
  {
    path: `${adminRoutes.EDIT}/:cnpj/:name/:period`,
    component: ClassRoomEditPage,
  },
  {
    path: adminRoutes.MONITOR_LIST,
    component: MonitorListComponent,
  },
  {
    path: adminRoutes.MONITOR_REGISTER,
    component: MonitorRegisterComponent,
  },
  {
    path: `${adminRoutes.MONITOR_EDIT}/:cpf`,
    component: MonitorEditComponent,
  },
  {
    path: adminRoutes.PARENT_REGISTER,
    component: ParentRegisterComponent,
  },
  {
    path: `${adminRoutes.PARENT_LIST}`,
    component: ParentListComponent,
  },
];
