export const adminRoutes = {
  REGISTER: 'classroom-register',
  LIST: 'classroom-list',
  EDIT: 'classroom-edit',
  MONITOR_REGISTER: 'monitor-register',
  MONITOR_LIST: 'monitor-list',
  MONITOR_EDIT: 'monitor-edit',
  PARENT_LIST: 'parent-list',
  PARENT_REGISTER: 'parent-register',
};
